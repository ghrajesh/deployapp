package com.dev.deployapp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.dev.deployapp.domain.Topic;

@Service
public class TopicsService {

	private List<Topic> topicList = new ArrayList<>(Arrays.asList(

            new Topic("_spring", "_Spring FrameWork", "_Spring Description"),
            new Topic("spring", "Spring FrameWork", "Spring Description"),
            new Topic("java", "Java FrameWork", "Java Description")

    ));

	public List<Topic> getAllTopic() {
		// TODO Auto-generated method stub
		return topicList;
	}

	public Topic getTopic(String id) {
		// TODO Auto-generated method stub
		return topicList.stream().filter(topic -> topic.getId().equals(id)).findFirst().get();
	}

	public void addTopic(Topic topic) {
		// TODO Auto-generated method stub
		topicList.add(topic);
	}

	public void updateTopic(Topic topic, String id) {
		// TODO Auto-generated method stub
		int counter = 0;
		for(Topic topic1:topicList) {
			
			if(topic1.getId().equals(id)) {
				topicList.set(counter, topic);
			}
			counter++;
		}
		
		
	}

	public void deleteTopic(String id) {
		// TODO Auto-generated method stub
		topicList.removeIf(topic -> topic.getId().equals(id));
	}



}
