package com.dev.deployapp.controller;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNGTesting {

	WebDriver driver;
	
	@BeforeTest
	public void beforeTest() {

		System.out.println("in Before Test");
		System.setProperty("webdriver.chrome.driver","driver/chromedriver.exe");
		driver = new ChromeDriver();
	}


	@AfterTest
	public void afterTest() {

		System.out.println("in after test");
		driver = null;

	}

	@Test
	public void testHome() {
		System.out.println("in test method");
		driver.get("http://localhost:9191/topics");
		String bodyText = driver.findElement(By.tagName("body")).getText();
		System.out.println("************" + bodyText);
		Assert.assertTrue(bodyText.contains(bodyText));
	}
}
