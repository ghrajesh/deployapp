/**
 * 
 */
package com.dev.deployapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dev.deployapp.domain.Topic;
import com.dev.deployapp.service.TopicsService;

/**
 * @author devendra.singh
 *
 */
@RestController
public class TopicController {

	/**
	 * 
	 */
	@Autowired
	TopicsService topicService;
	
	@RequestMapping("/topics")
	public List<Topic> allTopics(){
		
		return topicService.getAllTopic();
		
	}
	
	
	@RequestMapping("/topics/{id}")
	public Topic getTopics(@PathVariable("id") String id){
		
		return topicService.getTopic(id);
	}
	
	@RequestMapping(value="/topics", method = RequestMethod.POST)
	public void addTopics(@RequestBody Topic topic) {
		
		topicService.addTopic(topic);
	}
	
	@RequestMapping(value="/topics/{id}", method = RequestMethod.PUT)
	public void updateTopics(@RequestBody Topic topic, @PathVariable("id") String id) {
		
		topicService.updateTopic(topic, id);
	}
	
	
	@RequestMapping(value="/topics/{id}", method = RequestMethod.DELETE)
	public void deleteTopics(@PathVariable("id") String id) {
		
		topicService.deleteTopic(id);
	}
}
	


