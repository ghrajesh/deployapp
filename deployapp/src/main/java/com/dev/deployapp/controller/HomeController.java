
package com.dev.deployapp.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	
	@GetMapping("/")
	public String index() {
		
		return "index";
	}
	
	@GetMapping("/hello")
	public String sayhello(@RequestParam("name") String name, Model model) {
		
        model.addAttribute("name", name); 		
		return "hello";

	}
	
	@GetMapping("/home")
	public String home() {
		
         		
		return "<body>Welcome To Git The Application.</body>";

	}

}
