package com.dev.deployapp;

import java.io.File;
import java.net.URL;

import org.hibernate.validator.internal.util.privilegedactions.GetResources;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeleniumDemo {
	static ExtentTest test;
	static ExtentReports report;
	private static final String CHROMEDRIVER_EXE = "chromedriver.exe";
	 static WebDriver driver;
	static String driverFile;

	/*
	 * @BeforeClass public static void startTest() { driverFile = findFile(); driver
	 * = new ChromeDriver(); report = new
	 * ExtentReports(System.getProperty("user.dir") +
	 * "\\targets\\SeleniumDemoReport.html"); test =
	 * report.startTest("SeleniumDemoReport");
	 * 
	 * }
	 */

	@Test
	public void extentReportsDemo() {
		System.setProperty("webdriver.chrome.driver","d:/driver/chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.co.in");
		if (driver.getTitle().equals("Google")) {
			test.log(LogStatus.PASS, "Navigated to the specified URL");
		} else {
			test.log(LogStatus.FAIL, "Test Failed");
		}

		driver.quit();

		driver.close();

	}

	@AfterClass
	public static void endTest() {
		report.endTest(test);
		report.flush();
	}

	/*
	 * private static String findFile() { ClassLoader classLoader =
	 * Class.class.getClassLoader(); URL url =
	 * classLoader.getResource(CHROMEDRIVER_EXE); return url.getFile(); }
	 */
}
