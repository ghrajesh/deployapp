package com.dev.deployapp;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import junit.framework.Assert;
@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleJunittest {

	static ExtentTest test;
	static ExtentReports report;

	@BeforeClass
	public static void startTest() {
		report = new ExtentReports(System.getProperty("user.dir") + "\\targets\\SampleJunittestReport.html");
		test = report.startTest("SampleJunittesteport");
	}

	@Test
	public void Elementcheck() {

		System.setProperty("webdriver.chrome.driver","d:/driver/chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("http://localhost:9191/home");

		WebElement actualtest = driver.findElement(By.xpath("//*[text()='Welcome To Git Application.']"));

		Assert.assertEquals("Welcome To Git Application.", actualtest);

		driver.quit();

		driver.close();

	}

	@AfterClass
	public static void endTest() {
		report.endTest(test);
		report.flush();
	}
}
